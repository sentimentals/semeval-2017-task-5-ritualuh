import pickle as pkl
import pandas as pd
import json

def main():
    texts = None
    labels = None

    with open("texts_py3.pkl", "rb") as f:
        texts = pkl.load(f)

        texts = [" ".join(t) for t in texts]

    with open("labels_py3.pkl", "rb") as f:
        labels = pkl.load(f)

        labels = [l[0] for l in labels]

    df = pd.DataFrame({'text':texts, 'sentiment scores': labels})

    df.to_csv("puls.csv")


def append_compnames():
    df = pd.read_csv('puls.csv')

    df['company'] = [''] * df.shape[0]

    with open('sentnos_py3.pkl', "rb") as f:
        sentnos = pkl.load(f)
        sentnos = [s.split('--')[0] for s in sentnos]
        df['sentnos'] = sentnos
        df.set_index('sentnos', inplace=True)

    print('working with json')

    with open("tagged_entities.json") as f:
        line = f.readline()

        docnos = set([])
        compnames = {}
        i = 0
        while line:
            e = json.loads(line)

            docnos.add(e['docno'])
            compnames[e['docno']] = e['entities'][0]['name']

            line = f.readline()
            i+=1
            if i % 100 == 0:
                print(i)

    print('docnos loaded, starting instersect')

    for d in set(sentnos).intersection(docnos):
        df.loc[d, 'company'] = compnames[d]

    df.to_csv('puls.csv')


if __name__ == '__main__':
    # main()
    append_compnames()